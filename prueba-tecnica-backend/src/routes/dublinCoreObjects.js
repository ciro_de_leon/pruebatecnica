const { Router }=require('express');
const router=Router();

const { getAll,createDublinObject,updateDublinObject,deleteDublinObject,getDublinObject }=require('../controllers/dublinCoreController');

router.route('/').
   get(getAll).
   post(createDublinObject);

   
router.route('/:id')
    .get(getDublinObject)
    .put(updateDublinObject)
    .delete(deleteDublinObject);

module.exports=router;