const { Schema,model }=require('mongoose');

const dublinCoreSchema=new Schema({
   Titulo: String,
   Claves:[String],
   Descripcion:String,
   Fuente:String,
   tipoDeRecurso:String,
   Cobertura:{
       Ubicacion:{
          Latitud:Number,
          Longitud:Number
       },       
       fechaInicial:{
           type:Date,
           default:Date.now
       },
       fechaFinal:{
           type:Date,
           default:Date.now
       }
   }
});
module.exports=model('dublinCoreObject',dublinCoreSchema);