const dublinController={};
const dublinModel=require('../models/dublinCoreObject');

dublinController.getAll=async function(req,res){
    const all=await dublinModel.find();
    res.json(all);
 };
 dublinController.createDublinObject=async function(req,res){
    console.log(req.body); 
    const{ Titulo,Claves,Descripcion,Fuente,tipoDeRecurso,Cobertura }=req.body;
    
    const newDublinObject=new dublinModel({
        Titulo:Titulo,
        Claves:Claves,
        Descripcion:Descripcion,
        Fuente:Fuente,
        tipoDeRecurso:tipoDeRecurso,
        Cobertura:Cobertura
    }); 
    await newDublinObject.save();
    res.json({ message:'object created'});
 };
 dublinController.updateDublinObject=async function(req,res){
    const{ Titulo,Claves,Descripcion,Fuente,tipoDeRecurso,Cobertura }=req.body;
    const obj=await dublinModel.findByIdAndUpdate(req.params.id,{
        Titulo:Titulo,
        Claves:Claves,
        Descripcion:Descripcion,
        Fuente:Fuente,
        tipoDeRecurso:tipoDeRecurso,
        Cobertura:Cobertura
    });     
    res.json({ message:'object updated'});
 };
 dublinController.deleteDublinObject=async function(req,res){    
    await dublinModel.findByIdAndDelete(req.params.id);     
    res.json({message:'object deleted'});
 };
 dublinController.getDublinObject=async function(req,res){
    const obj=await dublinModel.findById(req.params.id);    
    res.json(obj);
 };

 module.exports=dublinController;