const mongoose=require ('mongoose');
//OBTENEMOS VARIABLE DE ENTORNO URL DATABASE
console.log(process.env.MONGODB_URI);
const URI=process.env.MONGODB_URI ? process.env.MONGODB_URI : 'mongodb+srv://elalumnopc:3145814781@pruebatecnica-us67t.mongodb.net/test?retryWrites=true&w=majority';

mongoose.connect(URI,{
    useNewUrlParser:true,
    useCreateIndex:true,
    useUnifiedTopology:true,
    useFindAndModify:false
});
const connection=mongoose.connection;
connection.once('open',()=>{
    console.log('DB is Connected');
})