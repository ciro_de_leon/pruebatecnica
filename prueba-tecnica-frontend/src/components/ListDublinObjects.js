import React, { Component } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
const urlBackendDomain=require('../urlBackendDomain');
const urlFrontendDomain=require('../urlFrontendDomain');


export default class ListDublinObjects extends Component {
    state={
        lista:[]
    }
    
    

    deleteDublingObject=async(index,e)=>{
        e.preventDefault();
        var uri=urlBackendDomain+'/api/dublinCoreObjects/'+index.obj._id;
        var res2=await axios.delete(uri);
        alert(res2.data.message);
        const res=await axios.get(urlBackendDomain+'/api/dublinCoreObjects');        
        this.setState({lista:res.data});
        
    }
    callUpdateDublinObject=(index,e)=>{        
        
        console.log(urlFrontendDomain+'/pruebaTecnica/DublinObject/Update/'+index.obj._id);        
        window.location.href=urlFrontendDomain+'/pruebaTecnica/DublinObject/Update/'+index.obj._id;        
    }

    async componentDidMount(){
        const res=await axios.get(urlBackendDomain+'/api/dublinCoreObjects');
        console.log(res.data);
        this.setState({lista:res.data});
     }
     styleWD={
         width:'100px'
     };

    render() {
        return (
            <div className="row">
             <div className="col-md-1"></div>
             <div className="col-md-10">
                 <table className="table">
                     <thead>
                         <tr>
                            <th scope="col">Tiulo</th>
                            <th scope="col">Claves</th>
                            <th scope="col">Descripcion</th>
                            <th scope="col">Fuente</th>
                            <th scope="col">Tipo De Recurso</th>
                            <th scope="col">Ubicacion Cob</th>
                            <th scope="col">Fecha Inicial Cob</th>
                            <th scope="col">Fecha Final Cob</th>
                            <th scope="col">Opciones</th>
                         </tr>
                     </thead>
                     <tbody>
                         {
                         this.state.lista.map(obj=>
                         
                         <tr key={obj._id}>
                         <td>{obj.Titulo}</td>
                         <td>
                             <select key={obj._id} style={this.styleWD}>
                             {obj.Claves.map(clave=>
                                <option key={clave}>{clave}</option>
                             )}
                             </select>
                         </td>
                         <td>{obj.Descripcion}</td>
                         <td>{obj.Fuente}</td>
                         <td>{obj.tipoDeRecurso}</td>
                         <td>({obj.Cobertura.Ubicacion.Latitud},{obj.Cobertura.Ubicacion.Longitud})</td>
                         <td>{obj.Cobertura.fechaInicial}</td>
                         <td>{obj.Cobertura.fechaFinal}</td>
                         <td>
                            <a href="/#"  onClick={(e)=>{this.deleteDublingObject({obj},e)}}>delete</a><br/>
                            <Link to={"/pruebaTecnica/DublinObject/Update/"+obj._id}>update</Link>
                         </td>
                         </tr>
                         )                         
                        }
                     </tbody>
                 </table>
             </div>
             <div className="col-md-1"></div>
            </div>
        )
    }
}
