import React, { Component } from 'react'
import axios from 'axios'
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";
const urlBackendDomain=require('../urlBackendDomain');

export default class UpdateDublinObject extends Component {

    state={
        ramClaves:[],
        fechaInicial:new Date(),
        fechaFinal:new Date(),
        id:''
    }
    async componentDidMount(){
        console.log(this.props.match.params.id);
        const res=await axios.get(urlBackendDomain+'/api/dublinCoreObjects/'+this.props.match.params.id);
        console.log(res.data);
        document.getElementById('titulo').value=res.data.Titulo;
        document.getElementById('descripcion').value=res.data.Descripcion;
        document.getElementById('fuente').value=res.data.Fuente;
        document.getElementById('coberturaLatitud').value=res.data.Cobertura.Ubicacion.Latitud;
        document.getElementById('coberturaLongitud').value=res.data.Cobertura.Ubicacion.Longitud;
        document.getElementById("tipoDeRecurso").value=res.data.tipoDeRecurso;
            this.setState({
                ramClaves:res.data.Claves,
                fechaInicial:new Date(res.data.Cobertura.fechaInicial),
                fechaFinal:new Date(res.data.Cobertura.fechaFinal),
                id:this.props.match.params.id
            });
        
     }

    handleChange = fechaInicial => this.setState({ fechaInicial:fechaInicial })
    handleChange2 = fechaFinal => this.setState({ fechaFinal:fechaFinal })

    onClickAddClave=(e)=>{
       e.preventDefault();
       var doc=document.getElementById("clave");
       var clave=doc.value; 
       console.log(this.state.ramClaves);
       var array=this.state.ramClaves;
       array.push(clave);
       this.setState({ramClaves:array});
       doc.value='';
       //console.log(array);
    }
    doNothing=(index,e)=>{
        e.preventDefault();
       console.log(this.state.ramClaves);
       var array=this.state.ramClaves;
       console.log(array.indexOf(index.ramClave),index);
       array.splice(array.indexOf(index.ramClave),1);
       this.setState({ramClaves:array}); 
    }
    onSubmit=async(e)=>{
        e.preventDefault();
        var titulo=document.getElementById('titulo').value;
        var descripcion=document.getElementById('descripcion').value;
        var fuente=document.getElementById('fuente').value;
        
        var select = document.getElementById("tipoDeRecurso");
        //var value=select.value;
        var tipoDeRecurso=select.options[select.selectedIndex].innerText;
        var strLatitud=document.getElementById('coberturaLatitud').value;
        var strLongitud=document.getElementById('coberturaLongitud').value;
        var latitud=parseInt(strLatitud);
        var longitud=parseInt(strLongitud);
        console.log(this.state.ramClaves);
        var res=await axios.put(urlBackendDomain+'/api/dublinCoreObjects/'+this.state.id,{
            Titulo : titulo,
            Claves: this.state.ramClaves,
            Descripcion: descripcion,
            Fuente: fuente,
            tipoDeRecurso:tipoDeRecurso,
            Cobertura:{
                Ubicacion:{
                    Latitud:latitud,
                    Longitud:longitud
                },
                fechaInicial: this.state.fechaInicial,
                fechaFinal: this.state.fechaFinal
            }
        });
        alert(''+res.data.message);        
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-3"></div>
                <div className="col-md-5">
                    <div className="card card-body">
                        <h1>Crear Recurso</h1>
                        <form>
                            <div className="form-group">
                                <br />
                                <label>Titulo :</label>
                                <br />
                                <input type="text" name="titulo" id="titulo" placeholder="titulo" className="form-control"></input>
                                <br />
                                <br />
                                <label>Clave :</label>
                                <br />
                                <input type="text" name="clave" id="clave" placeholder="clave" className="form-control"></input>
                                <br/>
                                <button onClick={this.onClickAddClave}>+</button>
                                <br />
                                <br />
                                <table>
                                   <thead>
                                       <tr>
                                          <th>Clave</th>
                                          <th>Opcion</th>
                                       </tr>
                                   </thead>
                                   <tbody>
                                       {
                                           this.state.ramClaves.map(ramClave =>
                                               <tr key={ramClave}>
                                                  <td>{ramClave}</td>
                                                  <td><a href="/#"  onClick={(e)=>{this.doNothing({ramClave},e)}}>delete</a></td>
                                               </tr>
                                            )
                                       }                                                             
                                   </tbody>
                                </table>
                                <br />
                                <label>Descripción :</label>
                                <br />
                                <input type="text" name="descripcion" id="descripcion" placeholder="descripcion" className="form-control"></input>
                                <br />
                                <br />
                                <label>Fuente :</label>
                                <br />
                                <input type="text" name="fuente" id="fuente" placeholder="fuente" className="form-control"></input>
                                <br />
                                <br />
                                <label>Tipo del Recurso :</label>
                                <br />
                                <select id="tipoDeRecurso" name="tipoDeRecurso" className="form-control">
                                    <option value="Testimonio">Testimonio</option>
                                    <option value="Informe">Informe</option>
                                    <option value="Caso">Caso</option>
                                </select>
                                <br />
                                <br />
                                <label>Cobertura</label>
                                <br />
                                <fieldset className="card card-body">
                                    <label>latitud</label>
                                    <br />
                                    <input type="text" name="coberturaLatitud" id="coberturaLatitud" className="form-control"></input>
                                    <br />
                                    <br />
                                    <label>longitud</label>
                                    <br />
                                    <input type="text" name="coberturaLongitud" id="coberturaLongitud" className="form-control"></input>
                                    <br />
                                    <br />
                                    <label>Fecha Inicial</label>
                                    <br />
                                    <DatePicker
                                        onChange={this.handleChange}
                                        selected={this.state.fechaInicial}
                                        id="fechaInicial"
                                        className="form-control"
                                     />
                                    <br />
                                    <br />
                                    <label>Fecha Final</label>
                                    <br />
                                    <DatePicker
                                        onChange={this.handleChange2}
                                        selected={this.state.fechaFinal}
                                        id="fechaFinal"
                                        className="form-control"
                                     />
                                </fieldset>
                                <button onClick={this.onSubmit}>Modificar</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div className="col-md-4"></div>
            </div>

        )
    }
}
