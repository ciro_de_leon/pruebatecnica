import React from 'react';
import { BrowserRouter as Router,Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import Navigation from './components/Navigation';
import ListDublinObjects from './components/ListDublinObjects';
import CreateDublinObject from './components/CreateDublinObject';
import UpdateDublinObject from './components/UpdateDublinObject';

function App() {
  return (    
    <Router>
      <Navigation/>
      <Route path="/pruebaTecnica/DublinObject/Create"  component={CreateDublinObject}></Route>
      <Route path="/pruebaTecnica/DublinObject/Update/:id" component={UpdateDublinObject}></Route>
      <Route path="/pruebaTecnica/DublinObject/" exact component={ListDublinObjects}></Route>      
    </Router>
  );
}

export default App;
